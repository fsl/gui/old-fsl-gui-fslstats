import { useState, useEffect, useRef, useCallback } from 'react'
import Box from '@mui/material/Box'
import LoadingButton from '@mui/lab/LoadingButton';
import { Button, Card, CardContent, Slider, Input, Collapse, Snackbar, FormControlLabel, Checkbox, Switch } from '@mui/material'
import { DataGrid } from '@mui/x-data-grid'
import { Container, CssBaseline, TextField, Typography, Grid, Divider } from '@mui/material'
import { Niivue } from '@niivue/niivue'
import { io } from "socket.io-client";
import "./App.css"

// get web socket connection info from url before rendering anything
let urlParams = new URLSearchParams(window.location.search)
let host = urlParams.get('host')
let socketServerPort = urlParams.get('socketServerPort')
let fileServerPort = urlParams.get('fileServerPort')
console.log(host, socketServerPort, fileServerPort)

// different socket clients for different widgets
const inFileSocket = io(`ws://${host}:${socketServerPort}`)
const runSocket = io(`ws://${host}:${socketServerPort}`)

const nv = new Niivue()

const NiiVue = ({url, sliceType=nv.sliceTypeMultiplanar}) => {
	// use url string (primative) since object equality will not work with effects
	const canvas = useRef()
	useEffect(() => {
		nv.attachToCanvas(canvas.current)
		nv.loadVolumes([{url:url}])
		nv.setSliceType(sliceType)
	}, [url])

	return (
		<Grid container item xs={12} m={2} alignItems='center' justifyContent='center' spacing={0} direction='row'>
			<canvas ref={canvas} height={200} width={640}></canvas>
	</Grid>
	)
}

function ResultTable({rows, cols}) {

	function setCrosshair(params) {
		let values = params['row']['value'].split(' ')
		let valuesNum = values.map((value) => {
			return parseFloat(value)
		})
		if (params['row']['statistic'] === 'max voxel (vox x y z)') {
			nv.scene.crosshairPos = nv.vox2frac(valuesNum)
			nv.updateGLVolume()
		} else if (params['row']['statistic'] === 'min voxel (vox x y z)') {
			nv.scene.crosshairPos = nv.vox2frac(valuesNum)
			nv.updateGLVolume()
		} else if (params['row']['statistic'] === 'centre of gravity (mm)') {
			nv.scene.crosshairPos = nv.mm2frac(valuesNum)
			nv.updateGLVolume()
		} else if (params['row']['statistic'] === 'centre of gravity (vox)') {
			nv.scene.crosshairPos = nv.vox2frac(valuesNum)
			nv.updateGLVolume()
		}
	}
	return (
		<div style={{ height: 400, width: '100%' }}>
      <DataGrid
        rows={rows}
        columns={cols}
        pageSize={20}
        rowsPerPageOptions={[20]}
        //checkboxSelection
        //disableSelectionOnClick
				onCellClick={(params, event)=>{setCrosshair(params)}}
      />
    </div>
	)
}

function InputField({text, updateOptsValue}) {
	// TODO: move this out of an effect
	useEffect(()=>{
		inFileSocket.on('files', (data) => {
			console.log(data)
			updateOptsValue('input', data[0])
		})
	}, [])

	return (
		<Grid container item xs={12} alignItems='center' spacing={0}>
			<Grid item xs={9}>
				<TextField 
					fullWidth 
					size="small" 
					label='input file' 
					onInput={(e) => {updateOptsValue('input', e.target.value)}}
					value={text}>
				</TextField>
			</Grid>
			<Grid item xs={3} sx={{justifyContent: 'flex-end'}}>
				<Button 
					variant='contained' 
					style={{margin:0, marginLeft: 12}}
					onClick={()=>{inFileSocket.emit('files')}}
				>
					Choose
				</Button>
			</Grid>
		</Grid>
	)
}

function Title({text}) {
	return (
		<Typography variant='h5' sx={{margin: '20px'}}>
			{text}
		</Typography>
	)
}

function ActionButtons({commandString, isRunning, setIsRunning}) {
	return (
		<Grid container item xs={12} alignItems='center' justifyContent='center' spacing={0} direction='row'>
			<LoadingButton
				onClick={()=>{runSocket.emit('run', {'run': commandString}); setIsRunning(true)}}
        loading={isRunning}
        loadingPosition="end"
        variant="contained"
				style={{margin:0}}
      >
        Run
      </LoadingButton>
		</Grid>
	)
}

function CommandStringPreview({commandString}) {
	return (
		<Grid 
			container 
			item 
			xs={12}
			alignItems='center'
			justifyContent='center' 
			spacing={0}
			direction='row'
		>
			<Typography sx={{'fontFamily': 'Monospace'}}>
				{`${commandString}`}
			</Typography>
		</Grid>
	)
}

export default function FslStats() {
	const title = "FSL Stats"
  const defaultOpts = {
		'input': 		'input', 		// input file path
		'-r': 				true, // 0, 1 				robust min/max
		'-R': 				true, // 2, 3  				min/max
		'-e': 				true, // 4 						mean entropy
		'-E': 				true, // 5 						mean entropy (non zero)
		'-v': 				true, // 6, 7 				voxels/volume
		'-V': 				true, // 8, 9   			voxels/volume (non zero)
		'-m': 				true, // 10 					mean
		'-M': 				true, // 11 					mean (non zero)
		'-s': 				true, // 12 					std.dev.
		'-S': 				true, // 13 					std.dev. non(zero)
		'-x': 				true, // 14, 15, 16   x y z (max voxel)
		'-X': 				true, // 17, 18, 19   x y z (min voxel)
		'-c': 				true, // 20, 21, 22  	x y z (cog mm)
		'-C': 				true, // 23, 24, 25  	x y z (cog vox)
	}
	const [opts, setOpts] = useState(defaultOpts)
	// the boolean variable to show or hide the snackbar
	const [showSnackBar, setShowSnackBar] = useState(false)
	// the message, and message setter for the snackbar
	const [snackBarMessage, setSnackBarMessage] = useState('')
	const [commandString, setCommandString] = useState('')
	const [isRunning, setIsRunning] = useState(false)
	const [niivueImage, setNiivueImage] = useState('')
	const [result, setResult] = useState('')
	const [resultRows, setResultRows] = useState([])
	const [resultCols, setResultCols] = useState([
		{field: 'id', headerName: 'ID', hide:true},
		{field:'statistic', headerName: 'statistic', width: 300},
		{field:'value', headerName:'value', width:350}
	])
	
	if (host === null || socketServerPort === null || fileServerPort === null){
		setSnackBarMessage('unable to contact backend application')
	}

	inFileSocket.on('files', (data) => {
		updateOptsValue('input', data[0] ? data[0] : '')
		if (data[0] !== '') {
			setNiivueImage(`http://${host}:${fileServerPort}/file/?filename=${data[0]}`)
		}
	})

	runSocket.on('stdout', (data) => {
		console.log('stdout', data)
		setIsRunning(false)
		setResult(data['stdout'] !== '' ? data['stdout'] : result)
	})

	function handleSnackBarClose() {
		setShowSnackBar(false)
	}

	function showSnackBarIfMsg() {
		if (snackBarMessage !== '') {
			setShowSnackBar(true)
		}
	}
	// whenever the snackbar message changes, run the effect (e.g. show the snackbar to the user when the message is updated)
	useEffect(() => {showSnackBarIfMsg()}, [snackBarMessage])
	useEffect(() => {updateCommandString()}, [opts])

	function updateCommandString() {
		let command = 'fslstats '
		for (let [key, value] of Object.entries({...opts})) {
			if (value != null) {
				if (value == false){
					continue
				} else if (value == true) {
					// if true the just pass the key to set boolean bet values
					value = ''	
				}
				command += `${(key=='input' || key=='output') ? '': key} ${value} `
			}
		}
		setCommandString(command)
	}


	function handleMoreOptions() {
		setMoreOptions(!moreOptions)
	}


	function updateOptsValue(option, value) {
		setOpts(defaultOpts => ({
			...defaultOpts,
			...{[option]: value}
		}))
	}

	function parseResultString() {
		let resultArray = result.split(' ')
		console.log(resultArray)
		setResultRows([
			{id: 0, statistic: 'robust min', value: resultArray[0]},
			{id: 1, statistic: 'robust max', value: resultArray[1]},
			{id: 2, statistic: 'min', value: resultArray[2]},
			{id: 3, statistic: 'max', value: resultArray[3]},
			{id: 4, statistic: 'mean entropy', value: resultArray[4]},
			{id: 5, statistic: 'mean entropy (non zero voxels)', value: resultArray[5]},
			{id: 6, statistic: 'voxels', value: resultArray[6]},
			{id: 7, statistic: 'volume', value: resultArray[7]},
			{id: 8, statistic: 'voxels (non zero voxels)', value: resultArray[8]},
			{id: 9, statistic: 'volume (non zero voxels)', value: resultArray[9]},
			{id: 10, statistic: 'mean', value: resultArray[10]},
			{id: 11, statistic: 'mean (non zero voxels)', value: resultArray[11]},
			{id: 12, statistic: 'standard deviation', value: resultArray[12]},
			{id: 13, statistic: 'standard deviation (non zero voxels)', value: resultArray[13]},
			{id: 14, statistic: 'max voxel (vox x y z)', value: `${resultArray[14]} ${resultArray[15]} ${resultArray[16]}`},
			{id: 15, statistic: 'min voxel (vox x y z)', value: `${resultArray[17]} ${resultArray[18]} ${resultArray[19]}`},
			{id: 16, statistic: 'centre of gravity (mm)', value: `${resultArray[20]} ${resultArray[21]} ${resultArray[22]}`},
			{id: 17, statistic: 'centre of gravity (vox)', value: `${resultArray[23]} ${resultArray[24]} ${resultArray[25]}`},
		])
	}

	useEffect(()=>{parseResultString()}, [result])

  return (
		<Container component="main" style={{height: '100%'}}>
			<CssBaseline enableColorScheme />
			<Box sx={{
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'center',
				height: '100%'
				}}>
				<Title text='FSL Stats' />
				<Grid container spacing={2}>
					<InputField
						updateOptsValue={updateOptsValue}
						text={opts['input']}
					/>
					<ActionButtons
						commandString={commandString}
						isRunning={isRunning}
						setIsRunning={setIsRunning}
					/>
					<CommandStringPreview commandString={commandString}/>
					<NiiVue url={niivueImage}/>
					<Grid item container xs={12}>
						<ResultTable rows={resultRows} cols={resultCols} />
					</Grid>
					<Snackbar
						anchorOrigin={{horizontal:'center', vertical:'bottom'}}
						open={showSnackBar}
						onClose={handleSnackBarClose}
						message={snackBarMessage}
						key='fslStatsSnackBar'
						autoHideDuration={5000}
					/>
				</Grid>
			</Box>
		</Container>
  )
}
